import bats from './bats.png';
import './App.css';
import { useState } from 'react';

function App() {
  const [xPercentage, setXPercentage] = useState(0.5);
  const [yPercentage, setYPercentage] = useState(0.5);

  function onMouseMove(event) {
    setXPercentage(event.clientX / document.body.clientWidth);
    setYPercentage(event.clientY / document.body.clientHeight);
  }

  const imgStyle = {
    transform: `rotateY(${(xPercentage-0.5)*1.5}deg) rotateX(${(0.5-yPercentage)*1.5}deg)`,
  }

  return (
    <div className="App" onMouseMove={onMouseMove}>
      <header>
        <a>How we made this</a>
        <a>View source</a>
      </header>
      <main className="App-main">
        <img style={imgStyle} src={bats} className="App-logo" alt="logo" />
        <p>
          The ping pong table is <em>free</em>
        </p>
        <div className="actions">
          <div className="button">
            <span>View live data</span>
            <span class="material-icons-round">show_chart</span>
          </div>
          <div className="button">
            <span>Standings</span>
            <span class="material-icons-round">emoji_events</span>
          </div>
        </div>
      </main>
      <footer>
        Made with ❤️ by Marple
      </footer>
    </div>
  );
}

export default App;
